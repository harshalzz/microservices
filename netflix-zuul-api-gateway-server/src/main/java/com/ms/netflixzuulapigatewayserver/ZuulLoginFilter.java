package com.ms.netflixzuulapigatewayserver;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ZuulLoginFilter extends ZuulFilter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Object run() throws ZuulException {
        HttpServletRequest httpServletRequest= RequestContext.getCurrentContext().getRequest();
        logger.info("request -> {} request Uri -> {}",httpServletRequest,httpServletRequest.getRequestURI());
        return null;
    }

    @Override
    public boolean shouldFilter() {
        return false;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public String filterType() {
        return "pre";
    }
    
}
